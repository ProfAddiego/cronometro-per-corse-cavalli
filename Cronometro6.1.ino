//**************************************************************//
//  Titolo  : Cronometro MM SS CC                               //
//  Autore  : Lino Addiego                                      //
//  Date    : 02/09/22                                          //
//  Versione: 5.0                                               //
//  Note    : Codice per l'utilizzo di 6 74HC595 Shift Register //
//         per realizzare un cronometro START/STOP al centesimo //
//         L'azzeramento avviene premendo a lungo il pulsante   //
//         che viene usato anche per lo start e lo stop         //
//         Lo stop è attivabile solo dopo 3 sec. dopo lo start  //
//**************************************************************//


//Pin connesso al ST_CP del 74HC595 (pin 12)
int latchPin = 4;
//Pin connesso al SH_CP del 74HC595 (pin 11)
int clockPin = 2;
//Pin connesso al DS del 74HC595 (pin 14)
int dataPin = 6;
//Pin connesso al OE del 74HC95
int oePin = 5;
// Pin connesso al /SRCLR del 74HC595
int puls = 3;
// ingresso pulsante
int pulsold = 7;
//corrispondenza numero da visualizzare e accensione segmenti
//int pattern[10] = {160,246,97,100,54,44,40,230,32,36};
int pattern[10] = { 192, 249, 164, 176, 153, 146, 130, 248, 128, 144 };
// variabili delle cifre da rappresentare
int dm;  // pattern segmenti decine dei minuti
int um;  // pattern segmenti unità dei minuti
int ds;  // pattern segmenti decine dei secondi
int us;  // pattern segmenti unità dei secondi
int dc;  // pattern segmenti decine dei centesimi
int uc;  // pattern segmenti unità dei centesimi
int m;   // minuti
int s;   // secondi
int c;   // centesimi
int disp[6];
long pausa;
volatile unsigned long  Crono = 0;
volatile unsigned long  initCrono = 0;
volatile int flagpartito = 0;
int cont;

void setup() {
  //imposto i pins come uscite o ingressi
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(oePin, OUTPUT);
  pinMode(puls, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  pinMode(pulsold, INPUT_PULLUP);
  Serial.begin(9600);

  attachInterrupt(digitalPinToInterrupt(puls), cambiostato, FALLING);

for (int(i) = 5; i >= 0; i--) {
      //tengo il latchPin basso per tutto il tempo che carico i dati nel rei registri
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 192);
    }

    //riporto il pin del latch high (fronte di salita) per informare il chip che
    //non è più necessario ricevere informazioni e quindi visualizzare il dato ricevuto
    digitalWrite(latchPin, HIGH);

}

void loop() {

  if (flagpartito == 1) {            //se il cronometro è partitto
    Crono = millis() - (initCrono);  // controllo quanto tempo è trascorso



    // faccio le conversioni dai millisecondi trascorsi ai minuti, secondi e centesimi
    m = (Crono / 60000);  // intervallo trascorso / 60000 => ricavo i minuti
    s = ((Crono - m * 60000) / 1000);
    c = ((Crono - ((m * 60000) + (s * 1000))) / 10);
    //Serial.println (c);
    // converto i valori nei pattern binari per accendere i segmenti
    // dei display in modo da visualizzare le cifre corrispondenti
    disp[0] = pattern[dm];
    disp[1] = pattern[um];
    disp[2] = pattern[ds];
    disp[3] = pattern[us];
    disp[4] = pattern[dc];
    disp[5] = pattern[uc];

    dm = ((m / 10));
    um = (m % 10);

    ds = (s / 10);
    us = (s % 10);

    dc = (c / 10);
    uc = (c % 10);

    // invio ai 7 SIPO la serie di bit (48 bit) per controllare
    // i corrispondenti segmenti dei 7 display (0= acceso, 1= spento)

    for (int(i) = 5; i >= 0; i--) {
      //tengo il latchPin basso per tutto il tempo che carico i dati nel rei registri
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, disp[i]);
    }

    //riporto il pin del latch high (fronte di salita) per informare il chip che
    //non è più necessario ricevere informazioni e quindi visualizzare il dato ricevuto
    digitalWrite(latchPin, HIGH);
  }}

  void cambiostato() {
    
    if (flagpartito == 1 && Crono < 3000) {
      return ;}
      flagpartito = !flagpartito;
      /*digitalWrite(13, HIGH);
      Crono = 0 ;
      initCrono = millis(); */
     
  
    if (flagpartito == 0 && (millis() -Crono)  < 3000) {
      return;}
      digitalWrite(13, LOW);
      Crono = 0 ;
      initCrono = millis();
      digitalWrite(13, LOW);
    }            
  
